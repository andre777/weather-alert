package com.app.weatheralert.favouritelocations

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import com.app.weatheralert.R
import com.app.weatheralert.favouritelocationselector.FavouriteLocationSelectorActivity
import com.app.weatheralert.favouritelocationselector.FavouriteLocationSelectorAdapter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Tests for the favourite locations screen, the main screen which contains a list of favourite locations.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class FavouriteLocationListScreenTest {

    @Rule @JvmField
    val favouriteLocationListActivityTestRule = IntentsTestRule(FavouriteLocationListActivity::class.java)

    @Test
    fun clickFloatingActionButton_opensFavouriteLocationSelector() {
        onView(withId(R.id.fab_add)).perform(click())

        intended(IntentMatchers.hasComponent(FavouriteLocationSelectorActivity::class.java.name))
    }

    @Test
    fun addFavouriteLocations() {
        onView(withId(R.id.fab_add)).perform(click())

        onView(withId(R.id.location_list))
                .perform(
                        RecyclerViewActions.actionOnItemAtPosition
                        <FavouriteLocationSelectorAdapter.ViewHolder>(0, click()))
    }

}
