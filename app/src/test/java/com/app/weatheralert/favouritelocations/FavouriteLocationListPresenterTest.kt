package com.app.weatheralert.favouritelocations

import com.app.weatheralert.data.FavouriteLocationsRepository
import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.util.PreferencesHelper
import com.nhaarman.mockito_kotlin.KArgumentCaptor
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.verify

/**
 * Tests for the implementation of [FavouriteLocationListPresenter]
 */
class FavouriteLocationListPresenterTest {

    private val mockView = mock<FavouriteLocationListContract.View>()

    private val mockRepository = mock<FavouriteLocationsRepository>()

    private lateinit var presenter: FavouriteLocationListPresenter

    private val mockPreferencesHelper = mock<PreferencesHelper>()

    private val favouriteLocations = listOf(
            FavouriteLocation(name = "Hurzuf"),
            FavouriteLocation(name = "Novinki"),
            FavouriteLocation(name = "Gorkhā"))

    /**
     * [ArgumentCaptor] is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */

    private val successConsumerCaptor: KArgumentCaptor<(List<FavouriteLocation>) -> Unit> = argumentCaptor()

    private val errorConsumerCaptor: KArgumentCaptor<(Throwable) -> Unit> = argumentCaptor()

    @Before
    fun setupPresenter() {
        presenter = FavouriteLocationListPresenter(mockRepository, mockView, mockPreferencesHelper)
    }

    @Test
    fun requestWeatherFromRepositoryAndLoadIntoWindow() {
        presenter.getWeather()

        verify(mockRepository).getWeather(
                ArgumentMatchers.anySet(), successConsumerCaptor.capture(), errorConsumerCaptor.capture())
        successConsumerCaptor.firstValue.invoke(favouriteLocations)

        verify(mockView).setData(favouriteLocations)
    }

    @Test
    fun clickOnFab_StartsFavouriteLocationSelector() {
        presenter.startFavouriteLocationSelector()

        verify(mockView).openFavouriteLocationSelector()
    }

    @Test
    fun clickOnFavouriteLocation_ShowsDetail() {
        // Create a stub
        val favouriteLocation = FavouriteLocation(name = "Hurzuf", id = 707860)

        presenter.openFavouriteLocationDetails(favouriteLocation)

        verify(mockView)
                .showFavouriteLocationDetails("Hurzuf", 707860)
    }

}