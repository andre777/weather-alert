package com.app.weatheralert.data.models.forecast

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForecastList : Parcelable {
    @SerializedName("dt")
    @Expose
    var dt: Long = 0
    @SerializedName("main")
    @Expose
    var main: Main? = null
    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null
    @SerializedName("clouds")
    @Expose
    var clouds: Clouds? = null
    @SerializedName("wind")
    @Expose
    var wind: Wind? = null
    @SerializedName("snow")
    @Expose
    var snow: Snow? = null
    @SerializedName("sys")
    @Expose
    var sys: Sys_? = null
    @SerializedName("dt_txt")
    @Expose
    var dateText: String? = null

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(dt)
        dest.writeValue(main)
        dest.writeList(weather)
        dest.writeValue(clouds)
        dest.writeValue(wind)
        dest.writeValue(snow)
        dest.writeValue(sys)
        dest.writeValue(dateText)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<ForecastList> = object : Parcelable.Creator<ForecastList> {


            override fun createFromParcel(`in`: Parcel): ForecastList {
                val instance = ForecastList()
                instance.dt = `in`.readValue(Long::class.javaPrimitiveType!!.classLoader) as Long
                instance.main = `in`.readValue(Main::class.java.classLoader) as Main
                `in`.readList(instance.weather, Weather::class.java.classLoader)
                instance.clouds = `in`.readValue(Clouds::class.java.classLoader) as Clouds
                instance.wind = `in`.readValue(Wind::class.java.classLoader) as Wind
                instance.snow = `in`.readValue(Snow::class.java.classLoader) as Snow
                instance.sys = `in`.readValue(Sys_::class.java.classLoader) as Sys_
                instance.dateText = `in`.readValue(String::class.java.classLoader) as String
                return instance
            }

            override fun newArray(size: Int): Array<ForecastList?> {
                return arrayOfNulls(size)
            }

        }
    }

}
