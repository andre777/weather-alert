package com.app.weatheralert.data.models.location

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Coordinates : Parcelable {
    @SerializedName("lon")
    @Expose
    var longitude: Double = 0.toDouble()
    @SerializedName("lat")
    @Expose
    var latitude: Double = 0.toDouble()

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(longitude)
        dest.writeValue(latitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<Coordinates> = object : Parcelable.Creator<Coordinates> {

            override fun createFromParcel(`in`: Parcel): Coordinates {
                val instance = Coordinates()
                instance.longitude = `in`.readDouble()
                instance.latitude = `in`.readDouble()
                return instance
            }

            override fun newArray(size: Int): Array<Coordinates?> {
                return arrayOfNulls(size)
            }

        }
    }

}