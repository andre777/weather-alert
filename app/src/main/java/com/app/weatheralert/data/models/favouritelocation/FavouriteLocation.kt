package com.app.weatheralert.data.models.favouritelocation

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavouriteLocation(@SerializedName("coord") val coordinates: Coordinates? = null,
                             val weather: List<Weather>? = null,
                             val base: String? = null,
                             val main: Main? = null,
                             val visibility: Long = 0,
                             val wind: Wind? = Wind(),
                             val clouds: Clouds? = null,
                             val dt: Long = 0,
                             val sys: Sys? = null,
                             val id: Long = 0,
                             val name: String? = null,
                             val cod: Long = 0) : Parcelable
