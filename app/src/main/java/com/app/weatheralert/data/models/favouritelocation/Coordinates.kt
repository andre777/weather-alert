package com.app.weatheralert.data.models.favouritelocation

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Coordinates(
        @SerializedName("lon") val longitude: Double = 0.0,
        @SerializedName("lat") val latitude: Double = 0.0) : Parcelable