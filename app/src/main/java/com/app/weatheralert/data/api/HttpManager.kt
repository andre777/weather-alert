package com.app.weatheralert.data.api

import android.os.Environment
import android.util.Log
import com.app.weatheralert.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

/**
 * Class responsible for communicating with server
 */
internal open class HttpManager {

    fun <T> createService(service: Class<T>): T {

        val cacheSize = 10L * 1024L * 1024L

        val cache = Cache(File(Environment.getDownloadCacheDirectory(), "cache"), cacheSize)

        // logging
        val logging = HttpLoggingInterceptor { message -> Log.d(TAG, message) }
        logging.level = HttpLoggingInterceptor.Level.NONE

        val httpClient = OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor { chain ->
                    val original = chain.request()
                    val url = original.url().newBuilder()
                            .addQueryParameter(APP_ID, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                            .build()

                    // Request customization: add request headers
                    val request = original.newBuilder().url(url).build()
                    chain.proceed(request)
                }
                .addInterceptor(logging)
                .build()

        val retrofit = Retrofit.Builder()
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

        return retrofit.create(service)
    }

    companion object {

        private val TAG = HttpManager::class.java.name

        const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
        const val APP_ID = "appid"
    }
}
