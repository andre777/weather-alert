package com.app.weatheralert.data.api

/**
 * Compose HTTP requests
 */
internal class ComposerApiManager : HttpManager() {

    fun getWeather(id: Long) = createService(ComposerService::class.java).getWeather(id)

    fun getForecast(id: Long) = createService(ComposerService::class.java).getForecast(id)
}
