package com.app.weatheralert.data.models.forecast;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sys_ implements Parcelable {

    public final static Creator<Sys_> CREATOR = new Creator<Sys_>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Sys_ createFromParcel(Parcel in) {
            Sys_ instance = new Sys_();
            instance.pod = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Sys_[] newArray(int size) {
            return (new Sys_[size]);
        }

    };
    @SerializedName("pod")
    @Expose
    private String pod;

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pod);
    }

    public int describeContents() {
        return 0;
    }

}
