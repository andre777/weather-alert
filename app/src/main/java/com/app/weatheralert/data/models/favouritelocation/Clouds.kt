package com.app.weatheralert.data.models.favouritelocation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Clouds(val all: Long) : Parcelable