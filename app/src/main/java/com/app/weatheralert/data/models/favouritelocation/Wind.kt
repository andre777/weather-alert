package com.app.weatheralert.data.models.favouritelocation

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wind(val speed: Double = 0.0,
                @SerializedName("deg") val degrees: Double = 0.0)
    : Parcelable
