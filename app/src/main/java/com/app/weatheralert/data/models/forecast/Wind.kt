package com.app.weatheralert.data.models.forecast

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wind(val speed: Double, @SerializedName("deg") val degrees: Double) : Parcelable
