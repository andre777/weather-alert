package com.app.weatheralert.data

import com.app.weatheralert.data.api.ComposerApiManager
import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.data.models.forecast.Forecast
import com.app.weatheralert.data.models.location.Location
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.IOUtils.readLines
import java.io.InputStream
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit

/**
 * Concrete implementation to load weather forecast from a data source.
 */
// Singleton
object FavouriteLocationsDataSource : FavouriteLocationsRepository {

    val instance: FavouriteLocationsDataSource? = null

    private const val TIME_SPAN = 500

    override fun getWeather(ids: Set<String>,
                            success: (favouriteLocations: List<FavouriteLocation>) -> Unit,
                            error: (e: Throwable) -> Unit) {
        getFavouriteLocationObservable(ids)
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success::invoke, error::invoke)
    }

    private fun getFavouriteLocationObservable(ids: Set<String>): Observable<FavouriteLocation> {
        return Observable
                .fromIterable(ids)
                .map(String::toLong)
                .flatMap { ComposerApiManager().getWeather(it) }
    }

    override fun getLocations(stream: InputStream,
                              next: (locations: List<Location>) -> Unit,
                              error: (e: Throwable) -> Unit,
                              complete: () -> Unit): Disposable {
        return Observable
                // get Location models from static json file stored in assets folder
                .fromIterable<String>(readLines(stream, Charset.defaultCharset()))
                .map { line -> Gson().fromJson(line, Location::class.java) }
                // update UI every 0,5 second with new portion of data
                .buffer(TIME_SPAN.toLong(), TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ next.invoke(it) }, { error.invoke(it) }, { complete.invoke() })
    }

    override fun getForecast(id: Long,
                             success: (forecast: Forecast) -> Unit,
                             error: (e: Throwable) -> Unit) {
        ComposerApiManager()
                .getForecast(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success::invoke, error::invoke)
    }

}
