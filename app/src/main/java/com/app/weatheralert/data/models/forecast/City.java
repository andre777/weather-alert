package com.app.weatheralert.data.models.forecast;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City implements Parcelable {

    public final static Creator<City> CREATOR = new Creator<City>() {

        public City createFromParcel(Parcel in) {
            City instance = new City();
            instance.id = in.readLong();
            instance.name = in.readString();
            instance.coord = in.readParcelable(getClass().getClassLoader());
            instance.country = in.readString();
            instance.population = in.readLong();
            instance.sys = in.readParcelable(getClass().getClassLoader());
            return instance;
        }

        public City[] newArray(int size) {
            return (new City[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("population")
    @Expose
    private long population;
    @SerializedName("sys")
    @Expose
    private Sys sys;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(coord);
        dest.writeValue(country);
        dest.writeValue(population);
        dest.writeValue(sys);
    }

    public int describeContents() {
        return 0;
    }

}
