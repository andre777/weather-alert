package com.app.weatheralert.data.models.location

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Location : Parcelable {
    @SerializedName("_id")
    @Expose
    var id: Long = 0
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("coord")
    @Expose
    var coordinates: Coordinates? = null

    var isSelected: Boolean = false

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(id)
        dest.writeValue(name)
        dest.writeValue(country)
        dest.writeValue(coordinates)
        dest.writeValue(isSelected)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<Location> = object : Parcelable.Creator<Location> {

            override fun createFromParcel(`in`: Parcel): Location {
                val instance = Location()
                instance.id = `in`.readValue(Long::class.javaPrimitiveType!!.classLoader) as Long
                instance.name = `in`.readValue(String::class.java.classLoader) as String
                instance.country = `in`.readValue(String::class.java.classLoader) as String
                instance.coordinates = `in`.readValue(Coordinates::class.java.classLoader) as Coordinates
                instance.isSelected = `in`.readValue(Boolean::class.javaPrimitiveType!!.classLoader) as Boolean
                return instance
            }

            override fun newArray(size: Int): Array<Location?> {
                return arrayOfNulls(size)
            }

        }
    }
}