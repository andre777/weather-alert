package com.app.weatheralert.data.api


import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.data.models.forecast.Forecast

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

internal interface ComposerService {

    @GET(FORECAST_PATH)
    fun getForecast(@Query("id") id: Long): Observable<Forecast>

    @GET(WEATHER_PATH)
    fun getWeather(@Query("id") id: Long): Observable<FavouriteLocation>

    companion object {
        const val FORECAST_PATH = "forecast"
        const val WEATHER_PATH = "weather"
    }
}
