package com.app.weatheralert.data

import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.data.models.forecast.Forecast
import com.app.weatheralert.data.models.location.Location
import io.reactivex.disposables.Disposable
import java.io.InputStream

/**
 * Main entry point for accessing weather data.
 */
interface FavouriteLocationsRepository {

    fun getWeather(ids: Set<String>,
                   success: (favouriteLocations: List<FavouriteLocation>) -> Unit,
                   error: (e: Throwable) -> Unit)

    fun getLocations(stream: InputStream,
                     next: (locations: List<Location>) -> Unit,
                     error: (e: Throwable) -> Unit,
                     complete: () -> Unit): Disposable

    fun getForecast(id: Long,
                    success: (forecast: Forecast) -> Unit,
                    error: (e: Throwable) -> Unit)

}
