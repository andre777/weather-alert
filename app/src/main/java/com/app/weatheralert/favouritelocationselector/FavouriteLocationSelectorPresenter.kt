package com.app.weatheralert.favouritelocationselector

import com.app.weatheralert.data.FavouriteLocationsRepository
import com.app.weatheralert.util.PreferencesHelper
import io.reactivex.disposables.Disposable
import java.io.IOException
import java.io.InputStream

/**
 * Listens to user actions from the UI [FavouriteLocationSelectorActivity], retrieves the data and updates the
 * UI as required.
 */
class FavouriteLocationSelectorPresenter(
        private val repository: FavouriteLocationsRepository,
        private val view: FavouriteLocationSelectorContract.View,
        private val preferencesHelper: PreferencesHelper) :
        FavouriteLocationSelectorContract.Presenter {

    /**
     * Get data from local file with buffering
     */
    @Throws(IOException::class)
    override fun getLocations(inputStream: InputStream): Disposable {
        return repository.getLocations(inputStream,
                { view.updateData(it) }, { view.handleError(it) }, { view.completeReading() })
    }

    override fun saveSelectedLocations(values: Set<String>) {
        preferencesHelper.favouriteLocations = values
    }

    override fun returnBackToFavouriteLocationList() {
        view.startFavouriteLocationListActivity()
    }

}
