package com.app.weatheralert.favouritelocationselector

import com.app.weatheralert.NavigationPresenter
import com.app.weatheralert.NavigationView
import com.app.weatheralert.data.models.location.Location
import io.reactivex.disposables.Disposable
import java.io.IOException
import java.io.InputStream

/**
 * This specifies the contract between the view and the favouriteLocationDetailPresenter.
 */
interface FavouriteLocationSelectorContract {

    interface View : NavigationView {

        fun handleError(e: Throwable)

        fun completeReading()

        fun updateData(locations: List<Location>)

    }

    interface Presenter : NavigationPresenter {

        @Throws(IOException::class)
        fun getLocations(inputStream: InputStream): Disposable

        fun saveSelectedLocations(values: Set<String>)

    }

}
