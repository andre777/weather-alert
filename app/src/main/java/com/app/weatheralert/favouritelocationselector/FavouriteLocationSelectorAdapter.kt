package com.app.weatheralert.favouritelocationselector

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import com.app.weatheralert.R
import com.app.weatheralert.data.models.location.Location
import kotlinx.android.synthetic.main.location_list_content.view.*
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [Location]
 */
class FavouriteLocationSelectorAdapter : RecyclerView.Adapter<FavouriteLocationSelectorAdapter.ViewHolder>() {

    val values: MutableList<Location> = ArrayList()
    var selectedLocations: MutableSet<String> = HashSet()

    fun setData(locations: List<Location>) {
        values.addAll(locations)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = from(parent.context).inflate(R.layout.location_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindLocation(values[position])


    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindLocation(location: Location) {
            with(location) {
                itemView.name.text = location.name
                itemView.country.text = location.country

                // set checkbox while initializing
                itemView.country_checkbox.setOnCheckedChangeListener(null)

                itemView.country_checkbox.isChecked = location.isSelected

                itemView.country_checkbox.setOnCheckedChangeListener { _, isChecked ->
                    //set location status
                    location.isSelected = isChecked
                    if (isChecked) {
                        selectedLocations.add(location.id.toString())
                    } else {
                        selectedLocations.remove(location.id.toString())
                    }
                }
            }
        }
    }

}
