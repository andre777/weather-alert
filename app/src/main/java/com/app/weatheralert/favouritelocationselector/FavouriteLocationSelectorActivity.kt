package com.app.weatheralert.favouritelocationselector

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.app.weatheralert.R
import com.app.weatheralert.data.FavouriteLocationsDataSource
import com.app.weatheralert.data.models.location.Location
import com.app.weatheralert.favouritelocations.FavouriteLocationListActivity
import com.app.weatheralert.util.FavouriteLocationsPreferences
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_favourite_location_selector.*
import org.jetbrains.anko.intentFor
import java.io.IOException

/**
 * Provide user list of possible to choose locations
 */
class FavouriteLocationSelectorActivity : AppCompatActivity(), FavouriteLocationSelectorContract.View {

    private lateinit var presenter: FavouriteLocationSelectorContract.Presenter

    private val adapter = FavouriteLocationSelectorAdapter()

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_location_selector)

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        location_list.adapter = adapter

        progress_bar.show()

        presenter = FavouriteLocationSelectorPresenter(
                FavouriteLocationsDataSource,
                this,
                FavouriteLocationsPreferences(this))
    }

    public override fun onResume() {
        super.onResume()

        try {
            assets.open(CITY_LIST).use { inputStream ->
                // put Disposable to Disposables' collection
                compositeDisposable.add(presenter.getLocations(inputStream))
            }
        } catch (e: IOException) {
            Log.e(TAG, "Cannot get access to list of cities", e)
        }

    }

    /**
     * Dynamically update [FavouriteLocationSelectorAdapter] after coming each portion of data

     * @param locations List of Locations from file
     */
    override fun updateData(locations: List<Location>) {
        adapter.setData(locations)
    }

    override fun handleError(e: Throwable) {
        Log.e(TAG, "getLocations", e)
        completeReading()
    }

    /**
     * Hide progress bar after reading all data from file
     */
    override fun completeReading() {
        progress_bar.hide()
    }

    override fun onOptionsItemSelected(item: MenuItem) = if (item.itemId == android.R.id.home) {
        presenter.returnBackToFavouriteLocationList()
        true
    } else {
        super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() = presenter.returnBackToFavouriteLocationList()


    override fun onDestroy() {
        // clear and dispose Disposables' collection
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun startFavouriteLocationListActivity() {
        // Save user preferred location to local data storage
        presenter.saveSelectedLocations(adapter.selectedLocations)
        setResult(RESULT_OK)
        // This ID represents the Home or Up button. In the case of this
        // activity, the Up button is shown.
        navigateUpTo(intentFor<FavouriteLocationListActivity>())
    }

    companion object {

        private val TAG = FavouriteLocationSelectorActivity::class.java.name

        const val CITY_LIST = "city.list.json"
    }
}
