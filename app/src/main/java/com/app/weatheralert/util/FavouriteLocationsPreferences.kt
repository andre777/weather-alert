package com.app.weatheralert.util

import android.content.Context
import org.jetbrains.anko.defaultSharedPreferences

/**
 * Handy class for storing and retrieving user's preferred locations using SharedPreferences
 */
class FavouriteLocationsPreferences(context: Context) : PreferencesHelper {

    private val preferences = context.defaultSharedPreferences

    override var favouriteLocations: Set<String>
        set(value) = preferences.edit().putStringSet(CITY_ID, value).apply()
        get() = preferences.getStringSet(CITY_ID, setOf<String>())

    companion object {
        private const val CITY_ID = "CITY_ID"
    }

}
