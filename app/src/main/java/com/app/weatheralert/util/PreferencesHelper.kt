package com.app.weatheralert.util

interface PreferencesHelper {
    var favouriteLocations: Set<String>
}
