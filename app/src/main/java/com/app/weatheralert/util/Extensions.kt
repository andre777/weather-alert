package com.app.weatheralert.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(resource: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(resource, this, attachToRoot)

fun View.setVisible(show: Boolean = true) {
    visibility = if (show) View.VISIBLE else View.GONE
}

