package com.app.weatheralert

interface NavigationPresenter {
    fun returnBackToFavouriteLocationList()
}
