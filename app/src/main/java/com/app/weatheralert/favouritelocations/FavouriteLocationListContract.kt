package com.app.weatheralert.favouritelocations

import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation

/**
 * This specifies the contract between the view and the favouriteLocationDetailPresenter.
 */
interface FavouriteLocationListContract {

    interface View {

        fun setData(items: List<FavouriteLocation>)

        fun handleRequestError(e: Throwable)

        fun openFavouriteLocationSelector()

        fun showFavouriteLocationDetails(favouriteLocationName: String, favouriteLocationId: Long)

    }

    interface Presenter {

        fun getWeather()

        fun startFavouriteLocationSelector()

        fun openFavouriteLocationDetails(favouriteLocation: FavouriteLocation)

    }

}
