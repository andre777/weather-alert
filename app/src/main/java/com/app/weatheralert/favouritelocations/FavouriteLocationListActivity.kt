package com.app.weatheralert.favouritelocations

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import com.app.weatheralert.R
import com.app.weatheralert.data.FavouriteLocationsDataSource
import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.favouritelocationdetail.FavouriteLocationDetailActivity
import com.app.weatheralert.favouritelocationdetail.FavouriteLocationDetailFragment
import com.app.weatheralert.favouritelocationselector.FavouriteLocationSelectorActivity
import com.app.weatheralert.util.Constants.FAVOURITE_DETAIL_TITLE
import com.app.weatheralert.util.Constants.FAVOURITE_LOCATION_ID
import com.app.weatheralert.util.FavouriteLocationsPreferences
import com.app.weatheralert.util.setVisible
import kotlinx.android.synthetic.main.activity_favourite_location_list.*
import kotlinx.android.synthetic.main.favourite_location_list.*
import org.jetbrains.anko.intentFor

/**
 * Main activity of the app representing a list of Favourite Locations. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [FavouriteLocationDetailActivity] representing
 * forecast details by chosen location. On tablets, the activity presents the list of favourite
 * locations and forecast details side-by-side using two vertical panes.
 */
class FavouriteLocationListActivity : AppCompatActivity(), FavouriteLocationListContract.View {

    // Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
    private var twoPane: Boolean = false

    private lateinit var presenter: FavouriteLocationListPresenter

    private lateinit var adapter: FavouriteLocationListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_location_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        // Navigates to locations selector screen
        fab_add.setOnClickListener { presenter.startFavouriteLocationSelector() }

        presenter = FavouriteLocationListPresenter(
                FavouriteLocationsDataSource,
                this,
                FavouriteLocationsPreferences(this))

        adapter = FavouriteLocationListAdapter(presenter::openFavouriteLocationDetails)

        favourite_location_list.adapter = adapter

        favouritelocation_detail_container?.let {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getWeather()
    }

    override fun showFavouriteLocationDetails(favouriteLocationName: String, favouriteLocationId: Long) {
        if (twoPane) {
            startFavouriteLocationDetailsFragment(favouriteLocationId)
        } else {
            startFavouriteLocationDetailsActivity(favouriteLocationName, favouriteLocationId)
        }
    }

    /**
     * Open [FavouriteLocationSelectorActivity] page with list of possible locations where user
     * can choose preferred location.
     */
    override fun openFavouriteLocationSelector() =
            startActivityForResult(intentFor<FavouriteLocationSelectorActivity>(), LOCATION_REQUEST_CODE)

    private fun startFavouriteLocationDetailsFragment(favouriteLocationId: Long) =
            supportFragmentManager.beginTransaction()
                    .replace(R.id.favouritelocation_detail_container,
                            FavouriteLocationDetailFragment.newInstance(favouriteLocationId))
                    .commit()

    private fun startFavouriteLocationDetailsActivity(name: String, id: Long) =
            startActivity(intentFor<FavouriteLocationDetailActivity>(
                    Pair(FAVOURITE_DETAIL_TITLE, name),
                    Pair(FAVOURITE_LOCATION_ID, id)))

    /**
     * Update UI after data were retrieved

     * @param items Favourite locations from server
     */
    override fun setData(items: List<FavouriteLocation>) {
        progress_bar.setVisible(false)
        if (!items.isEmpty()) {
            adapter.setData(items)
            showEmptyView(false)
        } else {
            showEmptyView(true)
        }
    }

    private fun showEmptyView(show: Boolean) {
        empty_view.setVisible(show)
        favourite_location_list.setVisible(!show)
    }

    override fun handleRequestError(e: Throwable) {
        Log.e(TAG, "getWeather", e)

        progress_bar.visibility = GONE
        empty_view.apply {
            text = getString(R.string.no_data_available)
            visibility = VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == LOCATION_REQUEST_CODE && resultCode == RESULT_OK) {
            presenter.getWeather()
        }
    }

    companion object {

        private val TAG: String = FavouriteLocationListActivity::class.java.name

        private const val LOCATION_REQUEST_CODE = 1000
    }

}
