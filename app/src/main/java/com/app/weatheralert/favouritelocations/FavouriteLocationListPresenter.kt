package com.app.weatheralert.favouritelocations

import com.app.weatheralert.data.FavouriteLocationsRepository
import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.util.PreferencesHelper

/**
 * Listens to user actions from the UI [FavouriteLocationListActivity], retrieves the data and updates the
 * UI as required.
 */
class FavouriteLocationListPresenter(
        private val repository: FavouriteLocationsRepository,
        private val view: FavouriteLocationListContract.View,
        private val preferencesHelper: PreferencesHelper) :
        FavouriteLocationListContract.Presenter {

    /**
     * Request weather data from server and merging result into list

     * @param ids Id of favourite locations in String representation
     */
    override fun getWeather() =
            repository.getWeather(preferencesHelper.favouriteLocations, view::setData, view::handleRequestError)

    override fun startFavouriteLocationSelector() = view.openFavouriteLocationSelector()

    override fun openFavouriteLocationDetails(favouriteLocation: FavouriteLocation) =
            view.showFavouriteLocationDetails(favouriteLocation.name.orEmpty(), favouriteLocation.id)

}
