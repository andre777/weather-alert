package com.app.weatheralert.favouritelocations

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.weatheralert.R
import com.app.weatheralert.data.models.favouritelocation.FavouriteLocation
import com.app.weatheralert.util.inflate
import kotlinx.android.synthetic.main.favourite_location_item.view.*

class FavouriteLocationListAdapter(private val itemClick: (FavouriteLocation) -> Unit) :
        RecyclerView.Adapter<FavouriteLocationListAdapter.ViewHolder>() {

    private var values: List<FavouriteLocation> = emptyList()

    fun setData(favouriteLocations: List<FavouriteLocation>) {
        values = favouriteLocations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.favourite_location_item), itemClick)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bindLocation(values[position])

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View, private val itemClick: (FavouriteLocation) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bindLocation(favouriteLocation: FavouriteLocation) {
            with(itemView) {
                item_id.text = favouriteLocation.name

                val wind = favouriteLocation.wind
                val windText = "Wind for today\nDegrees: ${wind?.degrees}\nSpeed: ${wind?.speed}"
                item_content.text = windText
                setOnClickListener { itemClick(favouriteLocation) }
            }
        }
    }

}
