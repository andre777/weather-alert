package com.app.weatheralert.favouritelocationdetail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.app.weatheralert.R
import com.app.weatheralert.data.FavouriteLocationsDataSource
import com.app.weatheralert.favouritelocations.FavouriteLocationListActivity
import com.app.weatheralert.util.Constants.FAVOURITE_DETAIL_TITLE
import com.app.weatheralert.util.Constants.FAVOURITE_LOCATION_ID
import kotlinx.android.synthetic.main.activity_favourite_location_detail.*

/**
 * An activity representing a single Favourite Location detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [FavouriteLocationListActivity].
 */
class FavouriteLocationDetailActivity : AppCompatActivity() {

    private lateinit var presenter: FavouriteLocationDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_location_detail)

        setSupportActionBar(detail_toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            supportActionBar?.title = intent.getStringExtra(FAVOURITE_DETAIL_TITLE)

            val fragment = FavouriteLocationDetailFragment.newInstance(
                    intent.getLongExtra(FAVOURITE_LOCATION_ID, 0))

            supportFragmentManager.beginTransaction()
                    .add(R.id.favouritelocation_detail_container, fragment)
                    .commit()

            presenter = FavouriteLocationDetailPresenter(FavouriteLocationsDataSource, fragment)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = if (item.itemId == android.R.id.home) {
        presenter.returnBackToFavouriteLocationList()
        true
    } else {
        super.onOptionsItemSelected(item)
    }

}
