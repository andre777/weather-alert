package com.app.weatheralert.favouritelocationdetail

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.weatheralert.R
import com.app.weatheralert.data.models.forecast.Forecast
import com.app.weatheralert.favouritelocations.FavouriteLocationListActivity
import com.app.weatheralert.util.Constants.FAVOURITE_LOCATION_ID
import com.app.weatheralert.util.inflate
import kotlinx.android.synthetic.main.favourite_location_detail.*

/**
 * A fragment representing a single Favourite Location detail screen.
 * This fragment is either contained in a [FavouriteLocationListActivity]
 * in two-pane mode (on tablets) or a [FavouriteLocationDetailActivity]
 * on handsets.
 */
class FavouriteLocationDetailFragment : Fragment(), FavouriteLocationDetailContract.View {

    private lateinit var adapter: FavouriteLocationDetailAdapter

    private lateinit var favouriteLocationDetailPresenter: FavouriteLocationDetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // loads the detailed forecast for the chosen location
        arguments?.run {
            if (containsKey(FAVOURITE_LOCATION_ID)) {
                favouriteLocationDetailPresenter.requestForecast(getLong(FAVOURITE_LOCATION_ID))
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.favourite_location_detail)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        favourite_location_detail.layoutManager = GridLayoutManager(context, 2)
        adapter = FavouriteLocationDetailAdapter()
        favourite_location_detail.adapter = adapter
        progress_bar.show()
    }

    override fun setPresenter(presenter: FavouriteLocationDetailContract.Presenter) {
        this.favouriteLocationDetailPresenter = presenter
    }

    override fun setData(forecast: Forecast) {
        adapter.setData(forecast.list)
        progress_bar.hide()
    }

    override fun handleRequestError(throwable: Throwable) {
        Log.e(TAG, "getWeather", throwable)
    }

    override fun startFavouriteLocationListActivity() {
        activity?.navigateUpTo(Intent(context, FavouriteLocationListActivity::class.java))
    }

    companion object {

        private val TAG = FavouriteLocationDetailFragment::class.java.name

        fun newInstance(id: Long) = FavouriteLocationDetailFragment().apply {
            arguments = Bundle(1).apply { putLong(FAVOURITE_LOCATION_ID, id) }
        }

    }
}
