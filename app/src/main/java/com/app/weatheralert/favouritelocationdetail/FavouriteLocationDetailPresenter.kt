package com.app.weatheralert.favouritelocationdetail

import com.app.weatheralert.data.FavouriteLocationsRepository

/**
 * Listens to user actions from the UI [FavouriteLocationDetailFragment], retrieves the data and updates the
 * UI as required.
 */
class FavouriteLocationDetailPresenter(
        private val repository: FavouriteLocationsRepository,
        private val view: FavouriteLocationDetailContract.View) :
        FavouriteLocationDetailContract.Presenter {

    init {
        view.setPresenter(this)
    }

    /**
     * Request forecast data from server and update UI with result
     * in [FavouriteLocationDetailContract.View.setData]

     * @param id Id of preferred location
     */
    override fun requestForecast(id: Long) =
            repository.getForecast(id, view::setData, view::handleRequestError)

    override fun returnBackToFavouriteLocationList() = view.startFavouriteLocationListActivity()

}
