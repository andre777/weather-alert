package com.app.weatheralert.favouritelocationdetail

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.weatheralert.R
import com.app.weatheralert.data.models.forecast.ForecastList
import com.app.weatheralert.util.inflate
import kotlinx.android.synthetic.main.forecast_list_content.view.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [ForecastList]
 */
class FavouriteLocationDetailAdapter : RecyclerView.Adapter<FavouriteLocationDetailAdapter.ViewHolder>() {

    private var values: List<ForecastList> = ArrayList()

    fun setData(locations: List<ForecastList>) {
        values = locations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.forecast_list_content))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bindForecast(values[position])

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bindForecast(forecast: ForecastList) {
            with(forecast) {
                val dateText = forecast.dateText
                val dateTime = LocalDateTime.parse(dateText, DateTimeFormatter.ofPattern(DATE_PATTERN))
                itemView.text_date.text = String.format(Locale.getDefault(),
                        // The 0 means that the number will be zero-filled
                        // if it is less than two (in this case) digits.
                        "%s\n%02d:%02d",
                        dateTime.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault()),
                        dateTime.hour,
                        dateTime.minute)
                itemView.text_degrees.text = "Wind degrees: ${forecast.wind?.degrees}"
                itemView.text_speed.text = "Wind speed: ${forecast.wind?.speed}"
            }
        }
    }

    companion object {
        const val DATE_PATTERN = "yyyy-MM-dd HH:mm:ss"
    }

}