package com.app.weatheralert.favouritelocationdetail

import com.app.weatheralert.NavigationPresenter
import com.app.weatheralert.NavigationView
import com.app.weatheralert.data.models.forecast.Forecast

/**
 * This specifies the contract between the view and the presenter.
 */
interface FavouriteLocationDetailContract {

    interface View : NavigationView {

        fun setPresenter(presenter: Presenter)

        fun setData(forecast: Forecast)

        fun handleRequestError(throwable: Throwable)

    }

    interface Presenter : NavigationPresenter {

        fun requestForecast(id: Long)

    }
}
