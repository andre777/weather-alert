# Notes: #

### Technologies were chosen while implementing: ###

* Retrofit + RxJava for networking. This combination provides elegant type-safe HTTP communicating and has rich set of operators for structuring data. Also with RxJava has asynchronous processing of data using functional programming.

* Support libraries RecyclerView that provides smooth displaying of data and CardView as part of Material design conception.

* For communication to OpenWeather service specific API key is used. It is stored in /.gradle/gradle.properties. 

### Improvements that can be implemented: ###

* Cover code with unit tests;
* Store data to local database instead of big file;
* Make handling request and other possible errors more robust;
* Update look&feel of application for both handset/tablet versions. 